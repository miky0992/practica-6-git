package mx.unitec.moviles.practica6.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import mx.unitec.moviles.practica6.dao.ContacDao
import mx.unitec.moviles.practica6.model.Contact

@Database(entities = [Contact::class], version = 1)
abstract class ContactsDataBase : RoomDatabase() {
    abstract fun ContactDao(): ContacDao

    companion object {
        private const val DATABASE_NAME = "contacts_database"

        @Volatile
        private var INSTANCE: ContactsDataBase? = null

        fun getInstance(context: Context) : ContactsDataBase? {
            INSTANCE ?: synchronized(this ) {
                INSTANCE = Room.databaseBuilder(
                    context.applicationContext,
                    ContactsDataBase::class.java,
                    DATABASE_NAME
                ).build()
            }
            return INSTANCE
        }
    }
}