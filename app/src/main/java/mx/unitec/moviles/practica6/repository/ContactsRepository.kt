package mx.unitec.moviles.practica6.repository

import android.app.Application
import androidx.lifecycle.LiveData
import mx.unitec.moviles.practica6.dao.ContacDao
import mx.unitec.moviles.practica6.db.ContactsDataBase
import mx.unitec.moviles.practica6.model.Contact

class ContactsRepository(application: Application) {
    private val contacDao: ContacDao? = ContactsDataBase.getInstance(application)?.ContactDao()

    suspend fun insert(contact: Contact){
        contacDao!!.insert(contact)
    }

    fun getContacts(): LiveData<List<Contact>> {
        return contacDao!!.getOrderedAgenda()
    }
}